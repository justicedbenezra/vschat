package me.justicepro.vschat.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Random;
import java.util.Scanner;

public class Encryption
{
	
	public static byte[] encrypt(byte[] b, Random random)
	{
		byte[] bytes = new byte[b.length];
		
		for (int i = 0; i < b.length; i++)
		{
			int c = b[i];
			
			int r = random.nextInt(1024);
			
			for (int j = 0; j < r; j++)
			{
				c++;
				if (c == 256)
				{
					c = 0;
				}
			}
			
			bytes[i] = (byte) c;
		}
		
		return bytes;
	}
	
	public static byte[] decrypt(byte[] b, Random random)
	{
		byte[] bytes = new byte[b.length];
		
		for (int i = 0; i < b.length; i++)
		{
			int c = b[i];
			
			int r = random.nextInt(1024);
			
			for (int j = 0; j < r; j++)
			{
				c--;
				if (c == -1)
				{
					c = 255;
				}
			}
			
			bytes[i] = (byte) c;
		}
		
		return bytes;
	}
	
}