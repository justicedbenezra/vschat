package me.justicepro.vschat.Test;

import static org.junit.Assert.assertArrayEquals;

import java.util.Random;

import org.junit.jupiter.api.Test;

import me.justicepro.vschat.Utils.Encryption;

class TestEncryption
{
	
	@Test
	void encrypt()
	{
		byte[] bytes = new byte[] {1, 1, 5, 5, 5};
		Random random = new Random(77);
		
		byte[] encryptedBytes = Encryption.encrypt(bytes, random);
		
		assertArrayEquals(new byte[] {-26,  49, 51, 46, 56}, encryptedBytes);
	}
	
	@Test
	void decrypt()
	{
		byte[] bytes = new byte[] {1, 1, 5, 5, 5};
		Random random = new Random(77);
		
		byte[] encryptedBytes = new byte[] {-26,  49, 51, 46, 56};
		byte[] decryptedBytes = Encryption.decrypt(encryptedBytes, random);
		
		assertArrayEquals(decryptedBytes, bytes);
	}
	
}